<?php
/**
 * Plugin Name: Augmented Realtime Interface
 * Description: Some description here.
 * Version: 1.0
 * Author: Contextly Inc.
 * Author URI: http://contextly.com/
 * License: GPL2 or later
 */

// Prevent direct file access
if ( !defined( 'ABSPATH' ) ) exit;


define( 'ARI_VERSION', '1.0' );

require_once('includes/loader.php');

class Augmented_Realtime_App
{
	/**
	 * @var $loader Augmented_Realtime_Loader
	 */
	private $loader = null;

	function __construct()
	{
		$this->loader = new Augmented_Realtime_Loader();
	}

	/**
	 *
	 */
	public function init()
	{
		$this->loader->initModules();
	}

	/**
	 *
	 */
	public function load()
	{
		$this->loader->loadModules();
	}

}

// Run application
$app = new Augmented_Realtime_App();
add_action('init', array($app, 'init'));
add_action('admin_enqueue_scripts', array($app, 'load'));
