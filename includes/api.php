<?php
/**
 * User: Andrew Nikolaenko
 * Date: 9/14/15
 * Time: 10:13 AM
 */

class Augmented_Realtime_Api
{
	const API_PATH = "http://ari.contextly.com/api/";

	/**
	 * @return array
	 */
	protected function getHeaders()
	{
		return array(
			'Accept' => 'application/vnd.contextly+json',
			'Content-Type' => 'application/json'
		);
	}

	/**
	 * @param $method
	 * @param $url
	 * @param array $data
	 * @return Augmented_Realtime_Response
	 */
	protected function request( $method, $url, $data = array() )
	{
		$result = wp_remote_request( $url, array(
			'timeout'   => 30,
			'method'    => $method,
			'body'      => json_encode( $data ),
			'headers'   => $this->getHeaders(),
			'sslverify' => false,
		) );

		$response = new Augmented_Realtime_Response();

		if ( is_wp_error( $result ) ) {
			$response->setCode( -1 );
			$response->setError( $result->get_error_message() );
		} else {
			$response->setCode( $result['response']['code'] );
			$response->setBody( wp_remote_retrieve_body( $result ) );
		}

		return $response;
	}

	/**
	 * @param $method_path
	 * @param $data
	 * @return mixed
	 * @throws Augmented_Realtime_Api_Exception
	 */
	public function get( $method_path, $data )
	{
		$response = $this->request( 'GET', self::API_PATH . $method_path, $data );

		$result = @json_decode( $response->getBody() );
		if ($result === NULL) {
			throw new Augmented_Realtime_Api_Exception("Unable to decode JSON response from server.", $response);
		}

		return $result;
	}

}

/**
 * Response object.
 *
 * Class Augmented_Realtime_Response
 */
class Augmented_Realtime_Response
{
	protected $code = null;
	protected $error = null;
	protected $body = null;

	/**
	 * @return null
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * @param null $code
	 */
	public function setCode($code)
	{
		$this->code = $code;
	}

	/**
	 * @return null
	 */
	public function getError()
	{
		return $this->error;
	}

	/**
	 * @param null $error
	 */
	public function setError($error)
	{
		$this->error = $error;
	}

	/**
	 * @return null
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 * @param null $body
	 */
	public function setBody($body)
	{
		$this->body = $body;
	}

}

/**
 * API Exceptions.
 *
 * Class Augmented_Realtime_Api_Exception
 */
class Augmented_Realtime_Api_Exception extends Exception {

	/**
	 * @var Augmented_Realtime_Response|null
	 */
	protected $response;

	/**
	 * @return Augmented_Realtime_Response|null
	 */
	public function getApiResponse() {
		return $this->response;
	}

	/**
	 * @param string $message
	 * @param Augmented_Realtime_Response $response
	 */
	public function __construct($message = '', $response = NULL) {
		parent::__construct($message);
		$this->response = $response;
	}

	/**
	 * @return array
	 */
	protected function getPrintableDetails() {
		$details = array();

		$details['class'] = get_class($this) . '. Code: ' . $this->getCode();
		$details['message'] = 'Message: "' . $this->getMessage() . '"';
		$details['file'] = 'File: ' . $this->getFile() . ':' . $this->getLine();

		if (isset($this->response)) {
			$details['api-response'] = "Response:\n" . print_r( $this->response, true );
		}

		return $details;
	}

	/**
	 * @return string
	 */
	public function __toString() {
		$details = $this->getPrintableDetails();

		// Add call stack to the end.
		$details['stack'] = "Stack trace:\n" . $this->getTraceAsString();

		return implode("\n\n", $details);
	}

}
