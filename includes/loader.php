<?php
/**
 * User: Andrew Nikolaenko
 * Date: 9/3/15
 * Time: 3:51 PM
 */

require_once('utils.php');
require_once('api.php');
require_once('module_base.php');
require_once('module_metadata.php');
require_once('module_wikipedia.php');

class Augmented_Realtime_Loader {

	private $_modules = array();

	/**
	 * Augmented_Realtime_Loader constructor.
	 */
	public function __construct()
	{
		$this->_loadModules();
	}

	/**
	 * Add module object to list of available modules.
	 *
	 * @param $module
	 */
	private function addModule( $module )
	{
		$this->_modules[] = $module;
	}

	/**
	 * Load modules. At this point we want to load only one module.
	 */
	private function _loadModules()
	{
		$this->addModule( new Augmented_Realtime_Module_Metadata() );
		$this->addModule( new Augmented_Realtime_Module_Wikipedia() );
	}

	/**
	 * Init modules.
	 */
	public function initModules()
	{
		foreach ( $this->_modules as $module ) {
			/**
			 * @var $module IAugmented_Realtime_Module
			 */
			$module->init();
		}
	}

	/**
	 * Load modules stuff.
	 */
	public function loadModules()
	{
		foreach ( $this->_modules as $module ) {
			/**
			 * @var $module IAugmented_Realtime_Module
			 */
			if ( $module->is_load_available() ) {
				$module->load();
			}
		}
	}


}
