<?php

interface IAugmented_Realtime_Module
{
	/**
	 * Action executed due WP init action
	 *
	 * @return mixed
	 */
	public function init();

	/**
	 * Check if we can load module stuff on current page.
	 *
	 * @return boolean
	 */
	public function is_load_available();

	/**
	 * Action executed due WP load scripts action
	 *
	 * @return mixed
	 */
	public function load();

	/**
	 * Module unique name.
	 *
	 * @return mixed
	 */
	public function module_key();

}

abstract class Augmented_Realtime_Module_Base
{
	private $page = null;

	/**
	 * Build unique key.
	 *
	 * @param $key
	 * @return string
	 */
	private function build_option_key( $key )
	{
		return sprintf( "_ari_%s", $key );
	}

	/**
	 * @param $key
	 * @return mixed
	 */
	protected function get_option( $key, $default_value = null )
	{
		$page = $this->get_current_page();
		$value = get_post_meta( $page->ID, $this->build_option_key( $key ), true );

		if ( !$value ) {
			return $default_value;
		}

		return $value;
	}

	/**
	 * @param $key
	 * @param $value
	 * @return mixed
	 */
	protected function set_option( $key, $value )
	{
		$page = $this->get_current_page();
		return update_post_meta( $page->ID, $this->build_option_key( $key ), $value );
	}

	/**
	 * @param $key
	 * @param $value
	 * @return mixed
	 */
	protected function add_option( $key, $value )
	{
		if ( is_array($value) ) {
			$prev_value = $this->get_option( $key, array() );
			$value = array_merge( $prev_value, $value );
		}

		return $this->set_option( $key, $value );
	}

	/**
	 * Check if option contain any value.
	 *
	 * @param $key
	 * @return bool
	 */
	protected function is_option_set( $key )
	{
		if ( $this->get_option( $key ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Check if load method is available for model. We want to allow it only post edit page.
	 *
	 * @return bool
	 */
	public function is_load_available()
	{
		global $pagenow;

		if ( ( $pagenow == "post.php" || $pagenow == "post-new.php" ) && is_admin() ) {
			if ( $this->is_valid_page() ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check if post object is defined and it has ID field.
	 *
	 * @return bool
	 */
	protected function is_valid_page()
	{
		$post = $this->get_current_page();

		if ( $post && isset( $post->ID ) ) {
			return true;
		}
	}

	/**
	 * Get post object from global scope.
	 *
	 * @return mixed
	 */
	protected function get_current_page()
	{
		if ( $this->page == null ) {
			global $post;
			return $post;
		}

		return $this->page;
	}

	/**
	 * @param $page
	 */
	protected function set_current_page( $page )
	{
		$this->page = $page;
	}

	/**
	 * Method for load some scripts and init some variables.
	 *
	 * @return mixed
	 */
	protected function load()
	{
		wp_enqueue_script( 'ari-module-base', plugins_url( 'js/base/module-compiled.js', dirname(__FILE__) ),
			array( 'jquery' ), ARI_VERSION );

		global $post;
		if ( $post && isset( $post->ID ) ) {
			$data = array();
			$data['page_id'] = $post->ID;
			$data['title'] = $post->post_title;
			$data['url'] = get_permalink( $post->ID );
			$data['security'] = wp_create_nonce( "ari-security" );

			// Inis some JS values for AJAX script
			wp_localize_script(
				'ari-module-base',
				'Ari',
				array(
					'l10n_print_after' => 'Ari = ' . json_encode( $data ) . ';'
				)
			);
		}
	}

	/**
	 * Method for build request JSON.
	 *
	 * @param $title
	 * @param $content
	 * @return array
	 */
	protected function build_api_json( $title, $content )
	{
		$data = array(
			'data_type' => $this->module_key(),
			'payload' => array(
				'title' => $title,
				'content' => $content,
			)
		);

		return $data;
	}

	/**
	 * Execute AJAX calls.
	 */
	public function execute_ajax()
	{
		check_ajax_referer( "ari-security", "security" );

		$data = $_REQUEST;
		$request_json = $this->build_api_json( $data['title'], $data['content'] );

		try {
			$api = new Augmented_Realtime_Api();
			$response_json = $api->get( 'recommendations', $request_json );

			$this->set_current_page( get_post( $data['page_id'] ) );

			$this->ajax_handler( $response_json );
		} catch ( Augmented_Realtime_Api_Exception $e ) {
			print $e;
		}

		wp_die();
	}

	/**
	 * Default AJAX handler.
	 *
	 * @param $json
	 * @throws Exception if method is not implemented.
	 */
	public function ajax_handler($json)
	{
		throw new Exception('Method "ajax_handler" is not implemented.');
	}

}
