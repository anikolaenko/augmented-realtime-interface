<?php

class Augmented_Realtime_Module_Metadata extends Augmented_Realtime_Module_Base
	implements IAugmented_Realtime_Module {

	/**
	 * Get Module name.
	 *
	 * @return string
	 */
	public function module_key()
	{
		return "METADATA";
	}

	/**
	 * Register AJAX call
	 */
	public function init()
	{
		add_action( 'wp_ajax_ari_get_metadata', array($this, 'execute_ajax') );
		add_action( 'save_post', array( $this, 'save_post_meta'), 10, 2 );
		add_action( 'admin_menu', array( $this, 'remove_metaboxes' ) );
	}

	/**
	 * Load page JS and create admin metabox.
	 */
	public function load()
	{
		parent::load();

		// Load jquery UI libraries
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-autocomplete' );
		wp_enqueue_script( 'jquery-ui-widget' );

		// Load module JS and CSS
		wp_enqueue_script( 'ari-tagit', plugins_url( 'js/metadata/lib/tag-it.js', dirname(__FILE__) ), array( 'jquery-ui-widget' ), ARI_VERSION );
		wp_enqueue_style ( 'ari-tagit', plugins_url ( 'css/metadata/lib/jquery.tagit.css', dirname(__FILE__) ), array(), ARI_VERSION );
		wp_enqueue_style ( 'ari-tagit-theme', plugins_url ( 'css/metadata/lib/tagit.ui-zendesk.css', dirname(__FILE__) ), array(), ARI_VERSION );

		wp_enqueue_script( 'ari-module-metadata', plugins_url( 'js/metadata/module-compiled.js', dirname(__FILE__) ), array( 'jquery' ), ARI_VERSION );
		wp_enqueue_style ( 'ari-module-metadata', plugins_url( 'css/metadata/module.css', dirname(__FILE__) ), array(), ARI_VERSION );

		add_meta_box(
			'augmented_realtime_tags',
			__('Tags', 'ari-meta-tags'),
			array($this, 'metabox_html_tags'),
			null,
			'side',
			'default'
		);
		add_meta_box(
			'augmented_realtime_categories',
			__('Categories', 'ari-meta-categories'),
			array($this, 'metabox_html_categories'),
			null,
			'side',
			'default'
		);
	}

	/**
	 * Remove default metaboxes for edit/add tags and categories.
	 */
	public function remove_metaboxes()
	{
		remove_meta_box( 'categorydiv','post','normal' );
		remove_meta_box( 'tagsdiv-post_tag','post','normal' );
		remove_meta_box( 'formatdiv','post','normal' );
		remove_meta_box( 'rcMetaBox','post','normal' );
	}

	/**
	 * Get plain array of post tags.
	 *
	 * @return array
	 */
	private function get_post_tags()
	{
		$result = array();
		$page = $this->get_current_page();

		$recommendation_tags = $this->get_option( 'ari_tags', array() );
		$page_tags = wp_get_post_tags( $page->ID );

		foreach ( $page_tags as $tag ) {
			if ( $tag->name ) {
				$result[] = new Augmented_Realtime_Module_Metadata_Item(
					$tag->name,
					in_array( $tag->name, $recommendation_tags )
				);
			}
		}

		return $result;
	}

	/**
	 * Get plain array of post categories.
	 *
	 * @return array
	 */
	private function get_post_categories()
	{
		$result = array();
		$page = $this->get_current_page();

		$recommendation_categories = $this->get_option( 'ari_categories', array() );
		$page_categories = wp_get_post_categories( $page->ID );

		foreach ( $page_categories as $category_id ) {
			$category = get_category( $category_id );

			if ( $category->name ) {
				$result[] = new Augmented_Realtime_Module_Metadata_Item(
					$category->name,
					in_array( $category->name, $recommendation_categories )
				);
			}
		}

		return $result;
	}

	/**
	 * Display HTML for admin Tags metabox.
	 */
	public function metabox_html_tags()
	{
		$meta_html = $this->get_tagit_control_html( 'ari-tags-control', $this->get_post_tags() );

		echo '<div>' . $meta_html . '</div>';
	}

	/**
	 * Display HTML for admin Categories metabox.
	 */
	public function metabox_html_categories()
	{
		$meta_html = $this->get_tagit_control_html( 'ari-categories-control', $this->get_post_categories() );

		echo '<div>' . $meta_html . '</div>';
	}

	/**
	 * Build tagit HTML control with some items.
	 *
	 * @param $id
	 * @param $data
	 * @return string
	 */
	private function get_tagit_control_html( $id, $data )
	{
		$html = '<ul id="' . esc_attr( $id ) . '">';
		foreach ( $data as $item ) {
			/**
			 * @var $item Augmented_Realtime_Module_Metadata_Item
			 */
			$html .= '<li class="' . ( $item->isRecommendation() ? 'ari-recommendation' : '' ) . '">' .
				$item->getName() . '</li>';
		}
		$html .= '</ul>';

		return $html;
	}

	/**
	 * Update post meta: tags and categories.
	 *
	 * @param $page_id
	 * @param $page
	 */
	public function save_post_meta( $page_id, $page )
	{
		// Save post tags
		if ( isset( $_REQUEST['_ari_metadata_tags'] ) ) {
			$tags = $_REQUEST['_ari_metadata_tags'];

			// Check if we need to remove some existed tags
			$page_tags = wp_get_post_tags( $page->ID );
			foreach ( $page_tags as $tag ) {
				if ( !in_array( $tag->name, $tags ) ) {
					wp_remove_object_terms( $page->ID, $tag->name, 'post_tag' );
				}
			}

			// Add new tag if needed
			foreach ( $tags as $tag ) {
				if ( $tag && !has_tag( $tag, $page ) ) {
					wp_set_post_tags( $page_id, $tag, true );
				}
			}
		} else {
			wp_set_post_tags( $page_id, array() );
		}

		// Save post categories
		if ( isset( $_REQUEST['_ari_metadata_categories'] ) ) {
			$categories = $_REQUEST['_ari_metadata_categories'];

			$page_categories = wp_get_post_categories( $page->ID );
			foreach ( $page_categories as $category_id ) {
				$category = get_category( $category_id );

				if ( !in_array( $category->name, $categories ) ) {
					wp_remove_object_terms( $page->ID, $category->name, 'category' );
				}
			}

			$add_categories_ids = array();
			foreach ( $categories as $category ) {
				if ( $category && !has_category( $category, $page ) ) {
					// Find category id
					$category_id = get_cat_ID( $category );
					if ( !$category_id ) {
						$category_id = wp_create_category( $category );
					}

					// Append category to post
					if ( $category_id ) {
						$add_categories_ids[] = $category_id;
					}
				}
			}

			if ( count( $add_categories_ids ) > 0 ) {
				wp_set_post_categories( $page_id, $add_categories_ids, true );
			}
		} else {
			wp_set_post_categories( $page_id, array() );
		}
	}

	/**
	 * AJAX handler implementation.
	 *
	 * @param $json
	 */
	public function ajax_handler($json)
	{
		if ( isset( $json->payload ) ) {
			$api_tags = $json->payload->tags;
			$api_categories = $json->payload->categories;

			$this->set_option( 'ari_tags', $api_tags );
			$this->set_option( 'ari_categories', $api_categories );

			echo json_encode( array(
				'tags' => $api_tags,
				'categories' => $api_categories,
				)
			);
		}
	}

}

/**
 * Metatag item, we will use this class for tags and categories.
 *
 * Class Augmented_Realtime_Module_Metadata_Item
 */
class Augmented_Realtime_Module_Metadata_Item {

	private $name = null;
	private $recommendation = false;

	function __construct( $name, $is_recommendation = false )
	{
		$this->name = trim( $name );
		$this->recommendation = $is_recommendation;
	}

	/**
	 * @return null
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param null $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return boolean
	 */
	public function isRecommendation()
	{
		return $this->recommendation;
	}

	/**
	 * @param boolean $recommendation
	 */
	public function setRecommendation($recommendation)
	{
		$this->recommendation = $recommendation;
	}

}
