<?php
/**
 * User: Andrew Nikolaenko
 * Date: 10/2/15
 * Time: 9:40 AM
 */

class Augmented_Realtime_Module_Wikipedia extends Augmented_Realtime_Module_Base
	implements IAugmented_Realtime_Module {

	/**
	 * Get Module name.
	 *
	 * @return string
	 */
	public function module_key()
	{
		return "WIKIPEDIA";
	}

	/**
	 * Register AJAX call
	 */
	public function init()
	{
		add_action( 'wp_ajax_ari_get_wikipedia', array($this, 'execute_ajax') );
	}

	/**
	 * Load page JS and create admin metabox.
	 */
	public function load()
	{
		parent::load();

		// Load jquery UI libraries
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-dialog' );
		wp_enqueue_style ( 'wp-jquery-ui-dialog' );

		// Load module JS and CSS
		wp_enqueue_script( 'ari-module-wikipedia', plugins_url( 'js/wikipedia/module-compiled.js', dirname(__FILE__) ), array( 'jquery' ), ARI_VERSION );
		wp_enqueue_style ( 'ari-wikipedia-css', plugins_url ( 'css/wikipedia/module.css', dirname(__FILE__) ), array(), ARI_VERSION );

		add_meta_box(
			'augmented_realtime_wiki_urls',
			__('Wikipedia Resources', 'ari-wiki-urls'),
			array($this, 'metabox_wiki_urls'),
			null,
			'side',
			'default'
		);

		add_action( 'in_admin_footer', array($this, 'add_preview_dialog_html') );
	}

	/**
	 * Get plain array of post tags.
	 *
	 * @return array
	 */
	private function get_items()
	{
		return $this->get_option( 'ari_wiki_items', array() );
	}

	/**
	 * Display HTML for admin Wiki Urls metabox.
	 */
	public function metabox_wiki_urls()
	{
		$items = $this->get_items();

		$meta_html = '';
		foreach ($items as $item) {
			$meta_html .= sprintf( "<p><a href=\"#\" ari-preview-url=\"%s\" target=\"_blank\">%s</a></p>",
				$item->url, $item->name );
		}

		echo '<div id="ari-wikipedia-control">' . $meta_html . '</div>';
	}

	/**
	 * Add preview dialog HTML to admin footer.
	 */
	public function add_preview_dialog_html()
	{
		echo '<div id="preview-dialog"><div class="url-preview-frame-wrapper">
				<iframe id="url-preview-frame" class="url-preview-frame" src="about:blank" frameBorder="0"></iframe>
			  </div></div>';
	}

	/**
	 * AJAX handler implementation.
	 *
	 * @param $json
	 */
	public function ajax_handler($json)
	{
		if ( isset( $json->payload ) ) {
			$this->set_option( 'ari_wiki_items', $json->payload );

			ob_start();
			$this->metabox_wiki_urls();
			$metabox_html = ob_get_contents();
			ob_end_clean();

			echo json_encode( array(
					'html' => $metabox_html
				)
			);
		}
	}

}
