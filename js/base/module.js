
class Augmented_Realtime_Module_Base {

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        this.init();
        this.page_id = options.page_id;
        this.prev_content = "";
    }

    /**
     * Init function.
     */
    init() {

    }

    /**
     * Main method for call AJAX script.
     *
     * @param action
     * @param callback
     *
     * TODO: Rewrite this method for query style, so multiple modules can use this ajax cycle
     */
    executeAjax( action, callback ) {
        var self= this;
        var data = {
            action: action,
            page_id: this.page_id,
            title: this.getCurrentTitle(),
            content: this.getCurrentContent(),
            security: Ari.security
        };

        jQuery.post(
            ajaxurl,
            data,
            function( response ) {
                // Save current post content
                self.prev_content = self.getCurrentContent();

                // Execute response handler
                callback(response);
            },
            'json'
        );
    }

    /**
     * Get current post title
     * @return string
     */
    getCurrentTitle() {
        return jQuery('#title').val();
    }

    /**
     * Get current post content
     * @return {string}
     */
    getCurrentContent() {
        var content = '';

        if ( typeof tinyMCE !== 'undefined' && typeof tinyMCE.activeEditor !== 'undefined' && tinyMCE.activeEditor ) {
            content = tinyMCE.activeEditor.getContent({format : 'raw'});
        } else {
            if ( jQuery('#content').length ) {
                content = jQuery('#content').val();
            }
        }

        return content;
    }

    /**
     * Return previously saved content.
     *
     * @return string
     */
    getPrevContent() {
        return this.prev_content;
    }

    /**
     * Default stub for handle ajax response.
     *
     * @param response
     */
    handleResponse(response) {
        console.log('This method need te be implemented in child classes');
    }


    /**
     * Method for check if AJAX is allowed. At this point we have few restrictions:
     * - text shorter than 5000 words
     * - writer added 10 words or more
     * - text diff > 2%
     *  @return boolean
     */
    isAjaxAllowed(options) {
        var defaults = {
            max_words: 6000,
            min_new_words: 100,
            min_text_diff: 0.2
        };

        options = jQuery.extend({}, defaults, options || {});

        var current_text = this.getCurrentContent().trim();
        var cur_words_count = current_text.split(' ').length;

        if ( cur_words_count < options.max_words ) {
            var previous_text = this.getPrevContent().trim();
            var prev_words_count = previous_text.split(' ').length;

            // Check words difference
            if ( Math.abs( cur_words_count - prev_words_count ) > options.min_new_words ) {
                return true;
            } else {
                // Calculate texts diff value in percents
                var diff = Augmented_Realtime_Module_Utils.getTextDiff( current_text, previous_text );

                if ( diff > options.min_text_diff ) {
                    return true;
                }
            }
        }

        return false;
    }
}

class Augmented_Realtime_Module_Utils {

    /**
     * Calculate diff value between texts. Really this is value of Livenshtein distance.
     *
     * @param text_1
     * @param text_2
     * @return number
     */
    static getTextDiff( text_1, text_2 ) {
        if ( !text_1.length || !text_2.length ) {
            if (!text_2) return 0;
            return 100; // totally different
        }

        var text_1_length = text_1.length;
        var text_2_length = text_2.length;
        var cursor_1    = 0; // cursor for string 1
        var cursor_2    = 0; // cursor for string 2
        var lcss        = 0; // largest common subsequence
        var local_cs    = 0; // local common substring
        var trans       = 0; // number of transpositions ('ab' vs 'ba')
        var offset_arr  = []; // offset pair array, for computing the transpositions

        while ((cursor_1 < text_1_length) && (cursor_2 < text_2_length)) {
            if (text_1.charAt(cursor_1) == text_2.charAt(cursor_2)) {
                local_cs++;
                var isTrans=false;
                // see if current match is a transposition
                var i=0;
                while (i<offset_arr.length) {
                    var ofs=offset_arr[i];
                    if (cursor_1<=ofs.c1 || cursor_2 <= ofs.c2) {
                        // when two matches cross, the one considered a transposition is the one with the largest difference in offsets
                        isTrans=Math.abs(cursor_2-cursor_1)>=Math.abs(ofs.c2-ofs.c1);
                        if (isTrans) {
                            trans++;
                        } else {
                            if (!ofs.trans) {
                                ofs.trans=true;
                                trans++;
                            }
                        }
                        break;
                    } else {
                        if (cursor_1>ofs.c2 && cursor_2>ofs.c1) {
                            offset_arr.splice(i,1);
                        } else {
                            i++;
                        }
                    }
                }
                offset_arr.push({
                    c1:cursor_1,
                    c2:cursor_2,
                    trans:isTrans
                });
            } else {
                lcss+=local_cs;
                local_cs=0;
                if (cursor_1!=cursor_2) {
                    cursor_1=cursor_2 = Math.min(cursor_1, cursor_2);  // using min allows the computation of transpositions
                }
                // if matching characters are found, remove 1 from both cursors (they get incremented at the end of the loop)
                // so that we can have only one code block handling matches
                for ( var i = 0; i < 5000 && (cursor_1+i < text_1_length || cursor_2+i < text_2_length); i++ ) {
                    if ((cursor_1 + i < text_1_length) && (text_1.charAt(cursor_1 + i) == text_2.charAt(cursor_2))) {
                        cursor_1+= i-1;
                        cursor_2--;
                        break;
                    }
                    if ((cursor_2 + i < text_2_length) && (text_1.charAt(cursor_1) == text_2.charAt(cursor_2 + i))) {
                        cursor_1--;
                        cursor_2+= i-1;
                        break;
                    }
                }
            }
            cursor_1++;
            cursor_2++;

            // this covers the case where the last match is on the last token in list, so that it can compute transpositions correctly
            if ( (cursor_1 >= text_1_length) || (cursor_2 >= text_2_length) ) {
                lcss+=local_cs;
                local_cs=0;
                cursor_1 = cursor_2 = Math.min(cursor_1,cursor_2);
            }
        }

        lcss+=local_cs;

        var lev_distance =  Math.max(text_1_length,text_2_length)- lcss + trans;
        var percent = ( lev_distance / Math.max( text_1_length, text_2_length ) ) * 100;

        return percent;
    }


}
