'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Augmented_Realtime_Module_Metadata = (function (_Augmented_Realtime_Module_Base) {
    _inherits(Augmented_Realtime_Module_Metadata, _Augmented_Realtime_Module_Base);

    function Augmented_Realtime_Module_Metadata() {
        _classCallCheck(this, Augmented_Realtime_Module_Metadata);

        _get(Object.getPrototypeOf(Augmented_Realtime_Module_Metadata.prototype), 'constructor', this).apply(this, arguments);
    }

    _createClass(Augmented_Realtime_Module_Metadata, [{
        key: 'init',
        value: function init() {
            Augmented_Realtime_Module_Metadata.initTagitComponents();
        }

        /**
         * Start module.
         */
    }, {
        key: 'run',
        value: function run() {
            var _this = this;

            if (this.isAjaxAllowed()) {
                // Use arrow function for process "this" properly
                this.executeAjax('ari_get_metadata', function (response) {
                    _this.handleResponse(response);
                });
            }
        }

        /**
         * AJAX response handler. Add suggested tags and categories.
         *
         * @param response
         */
    }, {
        key: 'handleResponse',
        value: function handleResponse(response) {
            if (response.tags) {
                jQuery("#ari-tags-control").find('.tagit-choice').remove();

                jQuery.each(response.tags, function (index, value) {
                    jQuery("#ari-tags-control").tagit("createTag", value, "ari-recommendation");
                });
            }
            if (response.categories) {
                jQuery("#ari-categories-control").find('.tagit-choice').remove();

                jQuery.each(response.categories, function (index, value) {
                    jQuery("#ari-categories-control").tagit("createTag", value, "ari-recommendation");
                });
            }
        }
    }], [{
        key: 'initTagitComponents',
        value: function initTagitComponents() {
            jQuery("#ari-tags-control").tagit({
                fieldName: "_ari_metadata_tags[]",
                allowSpaces: true
            });
            jQuery("#ari-categories-control").tagit({
                fieldName: "_ari_metadata_categories[]",
                allowSpaces: true
            });
        }
    }]);

    return Augmented_Realtime_Module_Metadata;
})(Augmented_Realtime_Module_Base);

jQuery(document).ready(function () {
    if (typeof Ari !== 'undefined') {

        var module = new Augmented_Realtime_Module_Metadata(Ari);
        module.run();

        setInterval(function () {
            module.run();
        }, 10000);
    }
});

//# sourceMappingURL=module-compiled.js.map