
class Augmented_Realtime_Module_Metadata extends Augmented_Realtime_Module_Base {

    init() {
        Augmented_Realtime_Module_Metadata.initTagitComponents();
    }

    /**
     * Start module.
     */
    run() {
        if ( this.isAjaxAllowed() ) {
            // Use arrow function for process "this" properly
            this.executeAjax( 'ari_get_metadata', (response) => {this.handleResponse(response);} );
        }
    }

    /**
     * AJAX response handler. Add suggested tags and categories.
     *
     * @param response
     */
    handleResponse(response) {
        if ( response.tags ) {
            jQuery("#ari-tags-control").find('.tagit-choice').remove();

            jQuery.each( response.tags, function( index, value ) {
                jQuery("#ari-tags-control").tagit("createTag", value, "ari-recommendation" );
            });
        }
        if ( response.categories ) {
            jQuery("#ari-categories-control").find('.tagit-choice').remove();

            jQuery.each( response.categories, function( index, value ) {
                jQuery("#ari-categories-control").tagit("createTag", value, "ari-recommendation" );
            });
        }
    }

    static initTagitComponents() {
        jQuery("#ari-tags-control").tagit({
            fieldName: "_ari_metadata_tags[]",
            allowSpaces: true
        });
        jQuery("#ari-categories-control").tagit({
            fieldName: "_ari_metadata_categories[]",
            allowSpaces: true
        });
    }

}

jQuery(document).ready(function () {
    if ( typeof Ari !== 'undefined' ) {

        var module = new Augmented_Realtime_Module_Metadata(Ari);
        module.run();

        setInterval(function () {
            module.run();
        }, 10000);

    }
});
