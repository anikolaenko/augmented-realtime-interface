
class Augmented_Realtime_Module_Wikipedia extends Augmented_Realtime_Module_Base {

    /**
     * Start module.
     */
    run() {
        if ( this.isAjaxAllowed() ) {
            // Use arrow function for process "this" properly
            this.executeAjax( 'ari_get_wikipedia', (response) => {this.handleResponse(response);} );
        }
    }

    /**
     * AJAX response handler. Display wikipedia urls.
     *
     * @param response
     */
    handleResponse(response) {
        if ( response && response.html ) {
            jQuery('#ari-wikipedia-control').html( response.html );
            this.initPreview();
        }
    }

    /**
     * Init preview controls.
     */
    initPreview() {
        var self = this;

        jQuery('[ari-preview-url]').each(
            function () {
                var element = jQuery(this);
                element.bind(
                    'click',
                    function () {
                        var url = element.attr('ari-preview-url');
                        self.openPreviewDialog(url);
                        return false;
                    }
                );
            }
        );
    }

    /**
     * Init preview dialog.
     */
    initPreviewDialog() {
        jQuery('#preview-dialog').dialog({
            title: 'Preview',
            modal: true,
            autoOpen: false
        });
    }

    /**
     * Open preview dialog for some url.
     * @param url
     */
    openPreviewDialog(url) {
        jQuery('#url-preview-frame').attr('src', url);

        var options = {
            width: jQuery(window).width() - 40,
            height: jQuery(window).height() - 40
        };

        jQuery('#preview-dialog').dialog('option', options).dialog('open');
    }
}

jQuery(document).ready(function () {
    if ( typeof Ari !== 'undefined' ) {

        var module = new Augmented_Realtime_Module_Wikipedia(Ari);
        module.initPreviewDialog();
        module.initPreview();

        setInterval(function () {
            module.run();
        }, 10000);

    }
});
